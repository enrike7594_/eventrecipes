package eventsnfc.recipes;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import eventsnfc.recipes.objects.Recipe;

public class RecipeActivity extends AppCompatActivity {

    int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        String receta_string = (String) getIntent().getExtras().getString("json_recipe");

        JsonObject jsonRecetas = new Gson().fromJson(receta_string, JsonObject.class);

        Recipe recipe = new Recipe(jsonRecetas);

        TextView name = (TextView)findViewById(R.id.name_recipe);
        TextView steps = (TextView)findViewById(R.id.steps_recipe);
        TableLayout tableLayout = (TableLayout)findViewById(R.id.table_ingredients);
        ImageView imageView = (ImageView)findViewById(R.id.image_recipe);
        TextView link = (TextView)findViewById(R.id.original_recipe_link);

        recipe.fillIngredientsTable(tableLayout, width);
        name.setText(recipe.getName());
//        ingredients.setText(recipe.getIngredientsClear());
        steps.setText(recipe.getStepsClear());

        Picasso.get().load(recipe.getImage_url()).resize(width, 500)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.i("IMAGEN", "CARGADO");
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("IMAGEN", "ERROR");
                        e.printStackTrace();
                    }
                });

        if (recipe.getOriginal_url() != null){
            Spanned html = Html.fromHtml("<a href='"+recipe.getOriginal_url()+"'>See original post</a>");
            link.setMovementMethod(LinkMovementMethod.getInstance());
            link.setText(html);
        }
        else{
            link.setVisibility(View.INVISIBLE);
        }
    }

}
