package eventsnfc.recipes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import eventsnfc.recipes.adapters.RecipesAdapter;
import eventsnfc.recipes.objects.Ingredient;
import eventsnfc.recipes.objects.Recipe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    /*
    Declarar instancias globales
     */
    private ProgressDialog pd;
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializar Animes
        new MainActivity.JsonTask().execute("https://ws-prod.eventsnfc.com/sample/recipes.json");
    }

    private class JsonTask extends AsyncTask<String, String, String> {

        //Preparación de tareas: abrimos un ProgressDialog mientras los datos se cargan
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
        }

        //Tarea principal: abrir flujos y conexiones
        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                //Realizar la conexión a la API
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                //Abrir Streams y Buffers
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                StringBuilder sBuilder = new StringBuilder();

                String line = "";

                //Por cada línea guardamos el resultado en un String
                while ((line = reader.readLine()) != null) {
                    sBuilder.append(line);
                }

                stream.close();

                return sBuilder.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        //Tarea final: Cargar los datos y cerrar el ProgressDialog
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pd.isShowing()) {
                pd.dismiss();
            }

            try {

                final JsonArray jsonRecetas = new Gson().fromJson(result, JsonArray.class);

                final ArrayList<Recipe> recipes = new ArrayList<Recipe>();

                for (int i=0; i<jsonRecetas.size(); i++){

                    JsonObject json_receta = jsonRecetas.get(i).getAsJsonObject();

                    Recipe rec = new Recipe(json_receta);

                    recipes.add(rec);
                }

                // Obtener el Recycler
                recycler = (RecyclerView) findViewById(R.id.reciclador);
                recycler.setHasFixedSize(true);

                // Usar un administrador para LinearLayout
                lManager = new LinearLayoutManager(MainActivity.this);
                recycler.setLayoutManager(lManager);

                // Crear un nuevo adaptador
                adapter = new RecipesAdapter(recipes);
                recycler.setAdapter(adapter);


                final GestureDetector mGestureDetector = new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {
                    @Override public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });

                recycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean b) {}

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                        try {
                            View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                            if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {

                                int position = recyclerView.getChildAdapterPosition(child);

                                Intent intent = new Intent(MainActivity.this, RecipeActivity.class);
                                intent.putExtra("json_recipe", jsonRecetas.get(position).getAsJsonObject().toString());
//                                intent.putExtra("recipe", recipes.get(position));
                                startActivity(intent);


                                return true;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) { }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
