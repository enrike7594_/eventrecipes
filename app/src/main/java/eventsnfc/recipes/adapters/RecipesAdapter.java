package eventsnfc.recipes.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import eventsnfc.recipes.MainActivity;
import eventsnfc.recipes.R;
import eventsnfc.recipes.objects.Recipe;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Creado por Hermosa Programación
 */
public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.RecipeViewHolder> {
    private List<Recipe> items;

    public static class RecipeViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public ImageView imagen;
        public TextView nombre;
        public TextView difficulty;
        public TextView ingredients;

        public RecipeViewHolder(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            nombre = (TextView) v.findViewById(R.id.nombre);
            difficulty = (TextView) v.findViewById(R.id.difficult_card);
            ingredients = (TextView) v.findViewById(R.id.ingredients_card);
        }
    }

    public RecipesAdapter(List<Recipe> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item, viewGroup, false);
        return new RecipeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder viewHolder, int i) {

        Picasso.get().load(items.get(i).getImage_url()).resize(100, 100)
                .into(viewHolder.imagen, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.i("IMAGEN", "CARGADO");
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("IMAGEN", "ERROR");
                        e.printStackTrace();
                    }
                });

        viewHolder.nombre.setText(items.get(i).getName());
        viewHolder.difficulty.setText(items.get(i).getDifficulty());
        viewHolder.ingredients.setText(items.get(i).getIngredients().size()+" ingredients");
//        viewHolder.visitas.setText("Visitas:"+ String.valueOf(items.get(i).getSteps()));
    }
}
