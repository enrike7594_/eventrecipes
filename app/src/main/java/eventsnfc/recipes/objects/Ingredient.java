package eventsnfc.recipes.objects;

import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;

public class Ingredient {

    private String quantity;
    private String name;
    private String type;

    public Ingredient(String quantity, String name, String type){
        this.quantity = quantity;
        this.name = name;
        this.type = type;
    }

    public Ingredient(JsonObject jsonIngredient){
        this.quantity = jsonIngredient.get("quantity").getAsString();
        this.name = jsonIngredient.get("name").getAsString();
        this.type = jsonIngredient.get("type").getAsString();
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }




}
