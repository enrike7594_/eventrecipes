package eventsnfc.recipes.objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.google.gson.*;
import eventsnfc.recipes.RecipeActivity;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Recipe{

    private String name;
    private ArrayList<Ingredient> ingredients;
    private String[] steps;
    private int[] timers;
    private String image_url;
    private String original_url;

    public Recipe(String name, ArrayList<Ingredient> ingredients, String[] steps, int[] timers, String image_url, String original_url) {
        this.name = name;
        this.ingredients = ingredients;
        this.steps = steps;
        this.timers = timers;
        this.image_url = image_url;
        this.original_url = original_url;
    }

    public Recipe(JsonObject json_receta){

        //Name
        this.name = json_receta.get("name").getAsString();

        //Ingredients
        JsonArray array_ingredients = json_receta.get("ingredients").getAsJsonArray();
        ArrayList<Ingredient> ingredients = new ArrayList<>();

        for (int z=0; z<array_ingredients.size(); z++){
            JsonObject jsonIngredient = array_ingredients.get(z).getAsJsonObject();
            Ingredient temp = new Ingredient(
                    jsonIngredient
            );
            ingredients.add(temp);
        }

        this.setIngredients(ingredients);

        //Steps
        Gson gson = new Gson();
        JsonArray array_steps = json_receta.getAsJsonArray("steps");
        String[] steps = gson.fromJson(array_steps , String[].class);
        this.steps = steps;

        //timers
        JsonArray array_timers = json_receta.getAsJsonArray("timers");
        int[] timers = gson.fromJson(array_timers , int[].class);

        this.setTimers(timers);

        //ImageURL
        this.setImage_url(json_receta.get("imageURL").getAsString());

        //OriginalURL
        try {
            this.setOriginal_url(json_receta.get("originalURL").getAsString());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setIngredientsJson(JsonArray ingredientsJson){
        for (int i=0; i<ingredientsJson.size(); i++){
//            Ingredient temp = new Ingredient(
//
//            );
        }
    }

    public String[] getSteps() {
        return steps;
    }

    public void setSteps(String[] steps) {
        this.steps = steps;
    }

    public int[] getTimers() {
        return timers;
    }

    public void setTimers(int[] timers) {
        this.timers = timers;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getOriginal_url() {
        return original_url;
    }

    public void setOriginal_url(String original_url) {
        this.original_url = original_url;
    }

    public Bitmap getImage_url_Bitmap(){
        URL url = null;
        Bitmap bmp = null;
        try {
            url = new URL(this.image_url);
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bmp;

//        imageView.setImageBitmap(bmp);
    }

    public String getIngredientsClear(){
        String retorno = "";

        for (int i=0; i<ingredients.size(); i++){
            retorno += "- "+ingredients.get(i).getName()+"     "+ingredients.get(i).getType()+"     "+ingredients.get(i).getQuantity()+"\n";
        }

        return retorno;
    }

    public void fillIngredientsTable(TableLayout table, int screen_width){

        int width = (screen_width/3)-20;

        for (int i=0; i<ingredients.size(); i++){
            TableRow row = new TableRow(table.getContext());
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            row.setPadding(0,0,0,15);

            TextView textViewType = new TextView(table.getContext());
            TextView textViewName = new TextView(table.getContext());
            TextView textViewQuantity = new TextView(table.getContext());

            textViewType.setWidth(width);
            textViewName.setWidth(width);
            textViewQuantity.setWidth(width);

            textViewType.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            textViewName.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            textViewQuantity.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            String name_cap = ingredients.get(i).getName().substring(0, 1).toUpperCase() + ingredients.get(i).getName().substring(1);

            textViewType.setText(ingredients.get(i).getType());
            textViewName.setText(name_cap);
            textViewQuantity.setText(ingredients.get(i).getQuantity());

            row.addView(textViewType);
            row.addView(textViewName);
            row.addView(textViewQuantity);

            table.addView(row);

        }

    }

    public String getStepsClear(){
        String retorno = "";

        for (int i=0; i<steps.length; i++){
            retorno += "- "+steps[i]+"\n";
        }
        return retorno;
    }

    public String getDifficulty(){
        if(steps.length<5)
            return "Easy";
        if (steps.length>5 && steps.length<10)
            return "Normal";
        else
            return "Hard";
    }
}
